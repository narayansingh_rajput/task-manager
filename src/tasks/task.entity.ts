import { Exclude } from "class-transformer";
import { User } from "src/auth/user.entity";
import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { TaskStatus } from "./task-status.enum";

@Entity()
export class Task {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    description: string;

    @Column()
    status: TaskStatus;

    @ManyToOne((_type) => User, (user) => user.id, { eager: false })
    @Exclude({toPlainOnly:true})
    user: User;
    
    @CreateDateColumn({type: 'timestamp without time zone', default: 'NOW()'})
    createdAt: Date;

    @UpdateDateColumn({type: 'timestamp without time zone', onUpdate: 'NOW()',nullable: true })
    updatedAt:Date;
}